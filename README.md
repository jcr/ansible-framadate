# Ansible Role: Framadate

## Requirements

Compliant with :
- Debian 10 (Buster)
- Python 3+
- Python-apt

Other:
- Fact gathering should be allowed in ansible-playbook (By default)
- The role require the superuser privileges. The task should be used with remote_user root or with a sudo/su grant

## Role behavior

This role installs and configures Framadate. It installs too several requirements :
- Nginx
- PHP-FPM
- MariaDB
- Certbot (if Let's Encrypt certificate generation is enabled)

It is possible to deploy only framadate sources without requirements. Just set `framadate_deploy_requirements` variable at false.

### HTTPS certificates
By default, the role installs Certbot and generates HTTPS certificates. If you set `framadate_certbot` at false, Certbot won't be installed and a self-signed certificate will be generated with OpenSSL.

### Basic authentication
A .htpasswd file is created to secure admin section. To disable this fonctionality, set at false `framadate_enable_admin_basic_auth` variable.


## Role Variables

Available variables with defaults values are defined in `defaults/main.yml`.

## Example of configuration :
```yaml
# Framadate
framadate_domain_name: framadate.example.org
framadate_admin_user: 'admin'
framadate_admin_password: 'password'
framadate_app_name: 'Framadate'
framadate_admin_email: 'admin@example.org'
framadate_allowed_languages:
  fr: 'Français'
  es: 'Español'
  de: 'Deutsch'
framadate_smtp_host: 'smtp.example.org'
framadate_smtp_auth: false
framadate_smtp_username: 'mulder'
framadate_smtp_password: 'password'
framadate_smtp_secure: 'SSL'

# PHP-FPM
framadate_phpfpm_timezone: 'Europe/Belgrade'

# MariaDB
framadate_db_name: 'framadate'
framadate_db_user_name: 'mulder'
framadate_db_user_password: 'password'

```

## Misc/Troubleshooting
### White page after deployment
If after deployment by the playbook your Framadate display only a white page, you must reload writings in database via these steps:
1. Go to myframadate.org/admin
1. Click on Migration
1. Refresh web page via your browser
1. If after refresh all 14 steps ared in skipped status, it must be good now

## Credits

Role maintained by Jonas Chopin-Revel on License GNU-GPLv3.

Gitlab : https://framagit.org/jcr/ansible-framadate

**Share and improve it. It's free !**
